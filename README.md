Link to the development prototype:  [http://ishpip1prod.appspot.com/](http://ishpip1prod.appspot.com/)

Link to the branch for evaluation:  [https://bitbucket.org/ishpiads/adsp1/src](https://bitbucket.org/ishpiads/adsp1/src)



# 750-WORD DESCRIPTION: #

Throughout prototype development, the ISHPI team demonstrated our capability to provide responsive application design services using modern open-source technologies following Agile best practices such as: frequent time-boxed iteration; collaboration and frequent interaction with customers and users; and incorporating feedback and lessons learned into subsequent iterations.  By leveraging data and applying lessons learned, the team was able to improve, finishing Sprint-4 tickets with 2.7% less effort than planned without deferring scope.  The team successfully met project goals by combining customer input with team expertise, and deploying a prototype that advanced dramatically, resulting in a delighted customer.

## Launch/Sprint Planning: ##
Based on senior management analysis of project needs, ISHPI identified the team's leader (the Product Manager), team members, and support resources such as coaching, and allocated them to the project (Evidence#02_Exhibit-1).  The coach worked with ISHPI management and customer product owner to schedule a team launch for Friday afternoon:

* The product owner discussed needs for the application (including key functionality and non-functional attributes), overall goals, and success criteria (Evidence#03)
* ISHPI management discussed success criteria

The leader and coach led the team in internalizing the goals and identifying the team's approach for performing the work.  The team agreed on a Definition of Done, an overall approach to perform one sprint per day, and contemplated three or four sprints to build the prototype implementing the highest-priority functionality.  The team chose the technologies and underlying platforms to implement the prototype (Evidence#01), all of which are openly licensed and free of charge.  They decided to use JIRA to plan and track work.  Starting with the launch and throughout execution, each member tracked their actual effort.

The team identified key CM practices to be followed (Evidence#02_Exhibits-4,5).  The team identified the branching scheme in order to perform their work without impacting the other team members, and merging procedures for systematically integrating their work into the product while maintaining stability.  Some team members volunteered to perform critical start-up tasks over the weekend (as part of the first sprint).

On Monday morning, the team performed sprint planning for the first sprint.  Based on the customer goals, the team identified the highest-priority needs to be implemented.  The team collaborated to estimate each task, and then validated that the work could be performed within the 1-day timebox.  The coach captured these estimates in Excel, loaded them into JIRA, and started the sprint (Evidence#04).  (The team back-dated the start of the sprint to Friday evening, which unfortunately prevented JIRA from creating a planned burn-down line for Sprint 1.)

## Execution: ##
During each sprint, the team planned/performed the following engineering work (Evidence#05,#06,#08):

* All
    * Elaborate details of user stories
    * End-of-sprint tasks:  system test, usability test, documentation, demo, and deployment
* Sprint-1
    * Provision resources and research data/APIs
    * Construct prototype About page reflecting the proposed "look and feel"
    * Notes:  Issues related to the deployment environment resulted in skipping end-to-end system test, usability test, and deployment to production
* Sprint-2
    * Construct prototype Search and Results pages
    * Implement customer feedback on About page
    * Setup deployment mechanism 
    * Notes:  Ongoing issues related to the deployment environment resulted in deferring end-of-sprint tasks.  Scope of the Results page was reduced by deferring interface with the API.
* Sprint-3
    * Complete end-of-sprint tasks deferred from sprint 2
    * Enhance search results to retrieve data using APIs
    * Ensure compatibility with required browsers
    * Finish setting up deployment mechanism
    * Notes:  Deferred Adverse Events functionality and testing Safari.  User Researcher was fully available to perform usability testing.
* Sprint-4
    * Create a style guide by capturing style decisions, and apply conventions consistently throughout
    * Apply feedback not yet incorporated
    * Ensure compatibility with required devices, and Safari
    * Notes:  A possible bug was noted for high-priority future investigation

During user story elaboration cycles, the team focused on human-centered design by conducting interviews, documenting personas, and creating use cases (Evidence#06, Evidence#01_Exhibits-7,8).  The team validated their interpretation by conducting user demos and usability tests.  During the design, the multidisciplinary team performed peer reviews and set times for multiple scrum meetings in order to stay on track (Evidence#07).  During each scrum, the leader led the team in concisely stating work accomplished, identifying upcoming work, and coordinating interdependencies to resolve blockers.

## Restrospectives: ##
At the end of each sprint timebox, the team performed a retrospective (Evidence#10) by reviewing data (Evidence#08-#09) and identifying improvements.  Frequent iteration enabled the team to promptly incorporate improvements into subsequent sprints such as closing tickets more promptly, refining scrum timing, and refining branching conventions.
