var index_ns = index_ns || {};

index_ns._construct = function(){
	var ns = this;
	var viewModel;

	this.ViewModel = function(){
		var self = this;
		self.isTest = false;		
		
		/**
		 * An value observable bound to the main search term that the user entered. 
		 */
		self.searchTerm = ko.observable("");
		
		/**
		 * The Error text that is displayed when a user does not enter a value.
		 * look for data-bind="value: errorText" in the index page.
		 */
		self.errorText = ko.observable("")
		
		/**
		 * An observable click function look at index.html for data-bind="click: search"
		 * To see what it is bound too. It is currently bound to the main search button.
 		 */
		self.search = function(){
			if(self.searchTerm()){			
				self.errorText("");				
				if (!self.isTest){					
					document.location.href = "/searchresults.html?searchTerm=" + self.searchTerm();	
				} else {
					console.log("This is a test run we do not want to actually go to the URL");
				}				
			} else {		
				self.errorText("Please enter something into the search bar.");
			}					
		}
	}
}

index_ns._construct();