var searchresults_ns = searchresults_ns || {};
var queryLimit = "50";
var api_key="YA18Ua8ekx7V6ZI5WUxTCaRo1wBhqWTr539fjseh";

/*
	Results page requires a link back to the search page.
	Replace dummy API results with results obtained from a openfda.fda.gov.
	If no data is found (i.e. the provided query term does not appear in the database), there shall be an indicator letting the user know there was no information found. When multiple entries are found, they shall be listed with the number of occurrences. Adverse Events are nested objects. Any fields without values shall be omitted from the UI.
	All of these fields are from the https://api.fda.gov/drug/event.json dataset
	Drug name (from query)
	Shall search within drug name fields
	patient.drug.medicinal_product
	patient.drug.openfda.brand_name
	patient.drug.openfda.generic_name
	
	Drug Manufacturer
	patient.drug.openfda.manufacturer_name
	
	Generic Equivalents
	patient.drug.openfda.generic_name
	
	Adverse Effects TODO
	patient.reaction.reactionmeddrapt
	patient.reaction.reactionoutcome [convert the enumeration to text]
	Indications (Purpose)
	patient.drug.drugindication
	
	var res = str.replace("Microsoft", "W3Schools");
*/
searchresults_ns._construct = function() {
	var ns = this;
	var viewModel;
	
	this.ViewModel = function(searchTerm){
		var self = this;
		self.searchErrorMessage = ko.observable("");
		self.isErrorMessage = ko.observable(false);
		
		self.notFoundText = ko.observable("Searching...");
		
		setTimeout(function(){
			self.notFoundText("No Results Found!");
		}, 10000);
		
		self.medicinalSearch = null;
		self.brandSearch = null;
		self.genericBrandSearch = null;	
		self.manufacturers = ko.observableArray();		
		self.brandnames = ko.observableArray();		
		self.genericNames = ko.observableArray();
		
		self.brandNamesCount = ko.pureComputed(function() {
				return self.brandnames().length;
		}, self);
		
		self.genericNamesCount = ko.pureComputed(function() {
		    return self.genericNames().length;
		}, self);
		
		self.manufacturerNamesCount = ko.pureComputed(function() {
		    return self.manufacturers().length;
		}, self);
		
		/**
		 * Computed observable that returns true only when at least one
		 * category of results is greater than 0.
		 */
		self.isResultsFound = ko.observable(false);		
		self.isBrandNamesExpanded = ko.observable(true);		
		self.isGenericNamesExpanded = ko.observable(true);
		self.isManufacturersExpanded = ko.observable(true);
		
		/**
		 * An observable bound to what the user searched.  
		 * It is passed into the view model at the top.
		 */
		self.searchTitle = ko.observable(unescape(searchTerm));
		
		/**
		 * An value observable bound to the main search term that the user entered. 
		 */
		self.searchTerm = ko.observable("");
		
		/**
		 * The Error text that is displayed when a user does not enter a value.
		 * look for data-bind="value: errorText" in the index page.
		 */
		self.errorText = ko.observable("")
		
		/**
		 * An observable click function look at index.html for data-bind="click: search"
		 * To see what it is bound too. It is currently bound to the main search button.
 		 */
		self.search = function(){			
			if(self.searchTerm()){				
				self.errorText("");				
				if (!self.isTest){					
					document.location.href = "/searchresults.html?searchTerm=" + self.searchTerm();	
				} else {
					console.log("This is a test run we do not want to actually go to the URL");
				}				
			} else {		
				self.errorText("Please enter something into the search bar.");
			}					
		}
		
		/**
		 * A convenience function used to toggle whether or not a particular 
		 * results category is displayed on the page.
		 */
		self.toggle = function(value){
			if (value === "brand"){
				self.isBrandNamesExpanded(!self.isBrandNamesExpanded());
			} else if (value === "generic"){
				self.isGenericNamesExpanded(!self.isGenericNamesExpanded());
			} else if (value === "manufactuerer"){
				self.isManufacturersExpanded(!self.isManufacturersExpanded());
			}
		}
		
		/**
		 * A computed observable function for the brand name plus minus button. 
		 * It toggles the appropriate css class.
		 */
		self.brandPlus = ko.pureComputed(function() {
		    return self.isBrandNamesExpanded() ? "fa-minus-square" : "fa-plus-square";
		}, self);
		
		/**
		 * A computed observable function for the generic name plus minus button. 
		 * It toggles the appropriate css class.
		 */
		self.genericPlus = ko.pureComputed(function() {
		    return self.isGenericNamesExpanded() ? "fa-minus-square" : "fa-plus-square";
		}, self);
		
		/**
		 * A computed observable function for the manufacturer name plus minus button. 
		 * It toggles the appropriate css class.
		 */
		self.manufactuererPlus = ko.pureComputed(function() {
		    return self.isManufacturersExpanded() ? "fa-minus-square" : "fa-plus-square";
		}, self);
		
		/**
		 * A convenience function used to values to a map.
		 * It was created to remove duplicates in the search results of
		 * the query.
		 */
		self.addToMap = function(mapContainer, aryvalues){					
			$.map( aryvalues, function( val, i ) {			
				if (val in mapContainer) { // 1. is A in the list
					//Do nothing	
				} else {
					mapContainer[val] = true;
				}
			});
		}
		
		/**
		 * A convenience function used to push map values to an observable array.
		 * The function additionally sorts the array after populating it.
		 */
		self.pushToObservableAry = function(observableAry, mapContainer){
			for (key in mapContainer){
				observableAry.push(key);
			}
			
			observableAry.sort(function(left, right) {
				var l = left.toUpperCase();
				var r = right.toUpperCase();
				
				return l == r ? 0 : (l < r ? -1 : 1) });
		}
		
		/**
		 * Function that processes the results.data from an FDA query.
		 * It matches any entry on brandname and genericname. 
		 * It adds results to a map for removing duplicates.
		 * Then sorts, and pushes those results onto an observable array 
		 * which is bound to HTML page elements.
		 */
		self.processData = function(results){
			console.log("Processing data");			
			var genericMap = Object.create(null);
			var manufacturerMap = Object.create(null);
			var brandMap = Object.create(null);
			var unescapedSearchTerm = unescape(searchTerm).toUpperCase();			
			unescapedSearchTerm = unescapedSearchTerm.replace(/%26/g, '&');
			
			for (i in results){				
				var drugsAry = results[i].patient.drug;					
				for (x in drugsAry){												
					if (drugsAry[x].openfda){																
						var brandName = drugsAry[x].openfda.brand_name;
						var genericNames = drugsAry[x].openfda.generic_name;
						var manufactuererNames = drugsAry[x].openfda.manufacturer_name;
						
						for (y in brandName){							 
							if (unescapedSearchTerm == brandName[y].toUpperCase()) {
								//Add the values to a map so we can remove duplicate enties.
								self.addToMap(genericMap, genericNames);																	
								self.addToMap(manufacturerMap, manufactuererNames);									
							}								
						}
																				
						for (y in genericNames){							
							if (unescapedSearchTerm == genericNames[y].toUpperCase()){
								var brandNames = drugsAry[x].openfda.brand_name;
								//Add the values to a map so we can remove duplicate enties.
								self.addToMap(brandMap, brandNames);									
								self.addToMap(manufacturerMap, manufactuererNames);																		
							}
						}							
					}						
				}
			}				
			
			//push our maps out to their respective observable arrays.				
			self.pushToObservableAry(self.manufacturers, manufacturerMap);		
			self.pushToObservableAry(self.genericNames, genericMap);
			self.pushToObservableAry(self.brandnames, brandMap);
			
			if (self.brandNamesCount() > 0){
				self.isResultsFound(true);
			}				
			if(self.genericNamesCount() > 0){
				self.isResultsFound(true);
			}			
			if(self.manufacturerNamesCount() > 0){
				self.isResultsFound(true);
			}			
			self.notFoundText("No Results Found!");
			console.log("Done");
		}
		
		/**
		 * This query calls the openfda API for Adverse events on patient data. The
		 * query attempts to match on brand_name, generic_name and medicinal product.
		 * If any results are found, we parse through the data and display relevant
		 * information pertaining to the medicinal product brand name and generic name.
		 */
		$.getJSON( "https://api.fda.gov/drug/event.json?api_key=" + api_key 
				+ "&search=patient.drug.medicinalproduct:%22" + searchTerm.toUpperCase()
				+ "%22+patient.drug.openfda.brand_name:%22" + searchTerm.toUpperCase() 
				+ "%22+patient.drug.openfda.generic_name:%22" + searchTerm.toUpperCase()  
				+ "%22&limit=" + queryLimit + "&skip=0", function( data ) {
			
			if (data.error){
				self.searchErrorMessage(data.error.message);
				self.isErrorMessage(true);				
			} else {
				self.processData(data.results);				
			}
		}).fail(function(){
			console.log("failure");
			self.isResultsFound(false);
		});					
	}
}

searchresults_ns._construct();