/**
 * Use this method for obtaining information from restful end points.
 * 
 * @param url
 * @param callbackFunc
 */
function ajaxGet(url, callbackFunc){
	$.ajax({
		url: url,
		data: null,
		async: true,
		cache: false,
		type: "GET",
		dataType: "json",
		success: callbackFunc,
		error: function(jqXHR, textStatus, errorThrown){
            console.log('error: ' + textStatus);
        }
	}); 
}

/**
 * Use this method for obtaining information for template html files so that 
 * the page loads all at once with no delay and that they get cached as well
 * in the process.
 * 
 * @param url
 * @param callbackFunc
 */
function ajaxGetTemplate(url, callbackFunc){
	$.ajax({
		url: url,
		data: null,
		async: true,
		cache: true,
		type: "GET",
		dataType: "html",
		success: callbackFunc,
		error: function(jqXHR, textStatus, errorThrown){
            console.log('error: ' + textStatus);
        }
	}); 
}

/**
 * Use this method for updating records on restful end points.
 * 
 * @param url
 * @param jsonObj
 * @param callbackFunc
 */
function ajaxPut(url, jsonObj, callbackFunc){
	$.ajax({
		url: url,
		contentType: 'application/json',
		data: JSON.stringify(jsonObj),
		async: true,
		cache: false,
		type: "PUT",
		dataType: "json",
		success: callbackFunc,
		error: function(jqXHR, textStatus, errorThrown){
			console.log('error: ' + textStatus);
        }
	}); 
}

/**
 * Use this method for adding new records to the data store.
 * 
 * @param url
 * @param jsonObj
 * @param callbackFunc
 */
function ajaxPost(url, jsonObj, callbackFunc){
	$.ajax({
		url: url,
		contentType: 'application/json',
		data: JSON.stringify(jsonObj),
		async: true,
		type: "POST",
		dataType: "json",
		cache: false,
		success: callbackFunc,
		error: function(jqXHR, textStatus, errorThrown){
			console.log('error: ' + textStatus);
        }
	}); 
}

/**
 * Use this method for deleting records from restful end points.
 * 
 * @param url
 * @param jsonObj
 * @param callbackFunc
 */
function ajaxDelete(url, callbackFunc){
	$.ajax({
		url: url,
		contentType: 'application/json',
		data: null,
		async: true,
		cache: false,
		type: "DELETE",
		dataType: "json",
		success: callbackFunc,
		error: function(jqXHR, textStatus, errorThrown){
			console.log('error: ' + textStatus);
        }
	}); 
}

/**
 * Get a URL parameter
 * 
 * @param sParam
 * @returns
 */
function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}  