var badRequest = {
  "error": {
    "code": "BAD_REQUEST",
    "message": "Invalid parameter: skdip"
  }
}

var noResultsFound = {
  "error": {
    "code": "NOT_FOUND",
    "message": "No matches found!"
  }
}

QUnit.test("SearchTestCases", function(assert){
	var model = new index_ns.ViewModel();
	model.isTest = true;
	
	//Run a search that does not have any input in it.
	assert.strictEqual("", model.searchTerm())
	model.search()
	assert.strictEqual("Please enter something into the search bar.", model.errorText())
	
	//Run another search that has an actual search term in it.
	model.searchTerm("RANOLAZINE");
	assert.strictEqual("RANOLAZINE", model.searchTerm());	
	model.search()
	assert.strictEqual("", model.errorText())
});

QUnit.test("MapIntegrationTestCase", function(assert){	
	var set = Object.create(null)
	var t = ['DOXYCYCLINE', 'DOXYCYCLINE HYCLATE'];
	var t2 = ['DOXYCYCLINE', 'DOXYCYCLINE HYCLATE'];
	
	var model = new searchresults_ns.ViewModel('DOXYCYCLINE');
	//TODO fix this later
	//model.addToMap();
	assert.ok(true);
});

//TODO fix this later
/*QUnit.test("DisplayResultsTestCases", function (assert) {
	console.log(sampleDisplayData.results);
	var model = new searchresults_ns.ViewModel(sampleDisplayData.results);
	
	//Assert that observables get rendered correctly
	assert.strictEqual(model.fulfillexpeditecriteria(), 
			sampleDisplayData.results[0].fulfillexpeditecriteria);
	
	assert.strictEqual(model.receivedateformat(), 
			sampleDisplayData.results[0].receivedateformat);
	
	assert.strictEqual(model.patient.drug()[0].openfda.manufacturer_name()[0], 	
			"Gilead Sciences, Inc.");
	
	assert.strictEqual(model.patient.drug()[0].openfda.brand_name()[0], 	
			"RANEXA");
	
	assert.strictEqual(model.patient.drug()[0].openfda.generic_name()[0], 	
			"RANOLAZINE");
	
	assert.strictEqual(model.patient.drug()[0].medicinalproduct(), 	
			"RANOLAZINE");
	
	assert.strictEqual(model.patient.reaction()[0].reactionmeddrapt(), 
			"Death");
	
	assert.strictEqual(model.patient.reaction()[0].reactionoutcome(), 
			"5");
	
	assert.strictEqual(model.patient.drug()[0].drugindication(), 			
			"ANGINA PECTORIS");
	
	assert.notOk(model.isErrorMessage());
	
	//Assert that a bad request happened
	var model = new searchresults_ns.ViewModel(badRequest);	
	assert.strictEqual("Invalid parameter: skdip", model.searchErrorMessage());
	assert.ok(model.isErrorMessage());
	//Assert that no results were found
	var model = new searchresults_ns.ViewModel(noResultsFound);
	assert.strictEqual("No matches found!", model.searchErrorMessage());
	assert.ok(model.isErrorMessage());
	
});*/

