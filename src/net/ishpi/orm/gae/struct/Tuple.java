package net.ishpi.orm.gae.struct;

/**
 * Java class to capture a pair.
 * 
 * @author ishpidcs
 * 
 * @param <A>
 * @param <B>
 */
public class Tuple<A, B> {

	public A a;
	public B b;

	public Tuple() {
		// default no-arg constructor
	}

	public Tuple(A a, B b) {
		super();
		this.a = a;
		this.b = b;
	}

	/**
	 * Static constructor for <code>Tuple</code>.
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static <A, B> Tuple<A, B> create(A a, B b) {
		return new Tuple<A, B>(a, b);
	}
}