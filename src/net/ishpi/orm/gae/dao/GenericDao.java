package net.ishpi.orm.gae.dao;

import java.util.List;
import java.util.Map;

import net.ishpi.orm.gae.dao.crit.Criterion;
import net.ishpi.orm.gae.struct.Tuple;

import com.googlecode.objectify.Key;

/**
 * Generic objectify dao.
 * 
 * @author ishpidcs
 */
public interface GenericDao<T> {

	@SuppressWarnings("unchecked")
	public void delete(T... ts);

	public void delete(long id);

	public void delete(String id);

	// needs implementation in each concrete since Id is not known generally
	public T find(long id);

	public T find(String id);

	public T find(Key<T> key);

	@SuppressWarnings("unchecked")
	public Map<Key<T>, T> find(Key<T>... keys);

	public Tuple<List<T>, String> find(Criterion[] criteria,
			Class<?>[] groups);

	public List<T> findAll();

	//public Tuple<List<Key<T>>, String> findKeys(List<Criterion> criteria);

	// def findParents[P](criteria: Criterion*): Map[Key[P], P] Tuple2 String

	@SuppressWarnings("unchecked")
	public Map<Key<T>, T> persist(T... ts);
	// def persist(es: E*): Map[Key[E], E]

	// repeat task in transaction until success
	// def transact[R](task: GenericDao[E] => R)
}
