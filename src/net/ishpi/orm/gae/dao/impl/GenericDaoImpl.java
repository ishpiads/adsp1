package net.ishpi.orm.gae.dao.impl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.ishpi.orm.gae.dao.GenericDao;
import net.ishpi.orm.gae.dao.crit.Criterion;
import net.ishpi.orm.gae.dao.crit.Limit;
import net.ishpi.orm.gae.dao.crit.Offset;
import net.ishpi.orm.gae.struct.Tuple;

import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;

public class GenericDaoImpl<T> implements GenericDao<T> {

	private Class<T> clazz;

	public GenericDaoImpl(Class<T> clazz) {
		this.clazz = clazz;
		ObjectifyService.register(clazz);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void delete(T... ts) {
		ofy().delete().entities(ts).now();
	}

	@Override
	public void delete(long id) {
		ofy().delete().type(clazz).id(id).now();
	}

	@Override
	public void delete(String id) {
		ofy().delete().type(clazz).id(id).now();
	}

	@Override
	public T find(long id) {
		return ofy().load().type(clazz).id(id).now();
	}

	@Override
	public T find(String id) {
		return ofy().load().type(clazz).id(id).now();
	}

	@Override
	public T find(Key<T> key) {
		return ofy().load().key(key).now();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Key<T>, T> find(Key<T>... keys) {
		return ofy().load().keys(keys);
	}

	@Override
	public Tuple<List<T>, String> find(Criterion[] criteria, Class<?>[] groups) {
		// createQuery(criteria).iterator();
		QueryResultIterator<T> qri = createQuery(criteria).iterator();// ofy().load().group(groups).type(clazz).iterator();
		Tuple<List<T>, String> rv = new Tuple<List<T>, String>();
		rv.a = new ArrayList<T>();
		while (qri.hasNext())
			rv.a.add(qri.next());
		rv.b = qri.getCursor().toString();
		return rv;
	}

	@Override
	public List<T> findAll() {
		return ofy().load().type(clazz).list();
	}

	// @Override
	// public Tuple<List<Key<T>>, String> findKeys(Criterion[] criteria) {
	// // TODO Auto-generated method stub
	// return null;
	// }

	@SuppressWarnings("unchecked")
	@Override
	public Map<Key<T>, T> persist(T... ts) {
		return ofy().save().entities(ts).now();
	}

	public Query<T> createQuery(Criterion... criteria) {
		Query<T> q = ofy().load().type(clazz);
		for (Criterion c : criteria)
			// be sure to reassign query since it is immutable
			// (i.e., not the local variable, but the class instances
			// themselves)
			switch (c.getType()) {
			case limit:
				q = q.limit(((Limit) c).limit);
				break;
			case offset:
				q = q.offset(((Offset) c).offset);
				break;
			default:
				break;
			}
		return q;
	}
}