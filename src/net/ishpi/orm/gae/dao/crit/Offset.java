package net.ishpi.orm.gae.dao.crit;

/**
 * Criterion for specifying the starting offset of a search.
 * 
 * @author ishpidcs
 */
public class Offset implements Criterion {

	public int offset;

	private Offset(int offset) {
		this.offset = offset;
	}

	public static Offset create(int offset) {
		return new Offset(offset);
	}
	
	@Override
	public CriteriaType getType() {
		return CriteriaType.offset;
	}
}