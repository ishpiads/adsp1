package net.ishpi.orm.gae.dao.crit;

/**
 * Criterion for specifying the results limit of a search.
 * 
 * @author ishpidcs
 */
public class Limit implements Criterion {

	public int limit;

	private Limit(int limit) {
		this.limit = limit;
	}

	public static Limit create(int limit) {
		return new Limit(limit);
	}
	
	@Override
	public CriteriaType getType() {
		return CriteriaType.limit;
	}
}