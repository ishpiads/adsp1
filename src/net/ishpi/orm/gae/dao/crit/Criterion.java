package net.ishpi.orm.gae.dao.crit;

/**
 * Basic interface for criteria.
 * 
 * @author ishpidcs
 */
public interface Criterion {

	public enum CriteriaType {
		offset, limit
	}
	
	public CriteriaType getType();
}