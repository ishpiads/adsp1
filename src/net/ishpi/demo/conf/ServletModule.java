package net.ishpi.demo.conf;

import java.util.HashMap;
import java.util.Map;

import com.googlecode.objectify.ObjectifyFilter;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

/**
 * Servlet initialization class. Uses init-on-demand holder idiom.
 * 
 * @author ishpidcs
 */
public class ServletModule extends com.google.inject.servlet.ServletModule {

	private static final Map<String, String> PROPERTIES;

	static {
		PROPERTIES = new HashMap<String, String>();

		// location of root resources
		PROPERTIES.put(PackagesResourceConfig.PROPERTY_PACKAGES,
				"net.ishpi.demo.rs");

		// enable WADL
		PROPERTIES.put(ResourceConfig.FEATURE_DISABLE_WADL, "false");

		// pojo mapping
		PROPERTIES.put("com.sun.jersey.api.json.POJOMappingFeature", "true");
	}

	@Override
	protected void configureServlets() {
		// mapping /rs/* context through the Objectify Filter
		filter("/rs/*").through(ObjectifyFilter.class);

		// restful endpoints
		serve("/rs/*").with(GuiceContainer.class, PROPERTIES);

		// super.configureServlets();
	}
}