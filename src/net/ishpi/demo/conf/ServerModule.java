package net.ishpi.demo.conf;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.googlecode.objectify.ObjectifyFilter;

public class ServerModule extends AbstractModule {

	@Override
	protected void configure() {
		// fetch limit
		bind(Integer.class).annotatedWith(Names.named("limit")).toInstance(2);

		// objectify filter is singleton instance
		bind(ObjectifyFilter.class).in(Singleton.class);

		// daos
		// bind(SomeDao.class).to(SomeDaoImpl.class).asEagerSingleton();
	}
}