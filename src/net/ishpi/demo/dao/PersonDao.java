package net.ishpi.demo.dao;

import net.ishpi.demo.dao.impl.PersonDaoImpl;
import net.ishpi.demo.db.Person;
import net.ishpi.orm.gae.dao.GenericDao;

import com.google.inject.ImplementedBy;

@ImplementedBy(PersonDaoImpl.class)
public interface PersonDao extends GenericDao<Person> { /* empty */ }