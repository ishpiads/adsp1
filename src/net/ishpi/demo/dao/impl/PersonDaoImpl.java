package net.ishpi.demo.dao.impl;

import net.ishpi.demo.dao.PersonDao;
import net.ishpi.demo.db.Person;
import net.ishpi.orm.gae.dao.impl.GenericDaoImpl;

import com.google.inject.Singleton;

@Singleton
public class PersonDaoImpl extends GenericDaoImpl<Person> implements PersonDao {

	public PersonDaoImpl() {
		super(Person.class);
	}
}