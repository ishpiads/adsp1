package net.ishpi.demo.listener;

import net.ishpi.demo.conf.ServerModule;
import net.ishpi.demo.conf.ServletModule;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Stage;
import com.google.inject.servlet.GuiceServletContextListener;

/**
 * Uses init-on-demand holder idiom.
 * 
 * @author ishpidcs
 */
public class InitListener extends GuiceServletContextListener {

	private static class LazyInjector {
		private static final Injector INJECTOR = Guice.createInjector(
			Stage.PRODUCTION,		// mode
			new ServerModule(),		// dependency injection
			new ServletModule()		// controller paths
		);
	}

	@Override
	protected Injector getInjector() {
		return LazyInjector.INJECTOR;
	}
}