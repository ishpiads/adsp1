package net.ishpi.demo.db;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.IgnoreSave;
import com.googlecode.objectify.condition.IfDefault;

@Entity
public class Person implements Serializable {

	@Id
	public Long id;		
	public String firstName;
	public String lastName;
	public String email;
	
	public Person() {
		// default no-arg constructor
	}

	public Person(String firstName, String lastName, String email) {
		super();
		id = null;		
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}
	
	public void updateFields(Person newUpdate){
		this.firstName = newUpdate.firstName;
		this.lastName = newUpdate.lastName;
		this.email = newUpdate.email;
	}
}