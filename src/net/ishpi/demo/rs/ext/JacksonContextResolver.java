package net.ishpi.demo.rs.ext;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.googlecode.objectify.util.jackson.ObjectifyJacksonModule;

/**
 * Implement json serialization across persistence tier.
 * 
 * @author ishpidcs
 */
@Provider
@Produces({ MediaType.APPLICATION_JSON })
public class JacksonContextResolver implements ContextResolver<ObjectMapper> {

	private static class LazyMapper {
		private static final ObjectMapper MAPPER = new ObjectMapper()
				.registerModule(new ObjectifyJacksonModule())
				.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
				.configure(SerializationFeature.INDENT_OUTPUT, true);
	}

	@Override
	public ObjectMapper getContext(Class<?> clazz) {
		return LazyMapper.MAPPER;
	}
}