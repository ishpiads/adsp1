package net.ishpi.demo.rs;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import net.ishpi.orm.gae.dao.GenericDao;

import com.sun.jersey.api.NotFoundException;

public abstract class AbstractResource<T> {

	private static final String NOT_FOUND = "no %s with id: %s exists";

	private Class<T> clazz;

	public AbstractResource(Class<T> clazz, GenericDao<T> dao) {
		this.clazz = clazz;
		this.dao = dao;
	}

	/**
	 * Default method for retrieving singleton items.
	 * 
	 * @param id
	 * @param info
	 * @return
	 * @throws NotFoundException
	 */
	@Path("{id: [0-9]*}")
	@GET
	@Produces(APPLICATION_JSON)
	public T read(
		@PathParam("id") final long id,
		@Context UriInfo info) throws NotFoundException {
		T t = dao.find(id);
		if (t == null)
			throw new NotFoundException(String.format(NOT_FOUND, clazz.getSimpleName(), id));
		return t;
	}
	
	/**
	 * Default method for saving/updating items.
	 * 
	 * @param t
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@POST
	@Produces(APPLICATION_JSON)
	@Consumes(APPLICATION_JSON)
	public T save(T t) {
		return dao.persist(t).values().iterator().next();
	}

	protected GenericDao<T> dao;
}