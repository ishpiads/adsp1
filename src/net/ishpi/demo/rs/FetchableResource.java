package net.ishpi.demo.rs;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import net.ishpi.orm.gae.dao.GenericDao;
import net.ishpi.orm.gae.dao.crit.Criterion;
import net.ishpi.orm.gae.dao.crit.Limit;
import net.ishpi.orm.gae.dao.crit.Offset;
import net.ishpi.orm.gae.struct.Tuple;

import com.google.inject.Inject;

/**
 * Create a RESTful resource which permits fetching lists of entities specified
 * by an offset. The limit is configured through guice <code>ServerModule</code>.
 * 
 * @author ishpidcs
 * 
 * @param <T>
 */
public class FetchableResource<T> extends AbstractResource<T> {

	public FetchableResource(Class<T> clazz, GenericDao<T> dao) {
		super(clazz, dao);
	}

	/**
	 * Callback to define functionality for fetching from an offset.
	 * 
	 * @author ishpidcs
	 */
	public interface LoadCallback<T> {

		/**
		 * Specify functionality for loading entity <code>T</code> from given
		 * offset.
		 * 
		 * @param offset
		 * @return
		 */
		public Tuple<List<T>, String> load(int offset);
	}

	/**
	 * Fetch items wrt offset and <code>LoadCallback</code>.
	 * 
	 * @param offset
	 * @param callback
	 * @return
	 */
	public Tuple<Integer, List<T>> fetch(int offset, LoadCallback<T> callback) {
		// recursively find results until limit or no results
		return find(offset, new ArrayList<T>(), callback);
	}

	/**
	 * Recursive call to load entities.
	 * 
	 * @param offset
	 * @param rv
	 * @param callback
	 * @return
	 */
	private Tuple<Integer, List<T>> find(int offset, List<T> rv, LoadCallback<T> callback) {
		int size = rv.size();
		if (size >= limit)
			// limit reached
			return Tuple.create(offset, rv);
		else {
			// limit not reached
			// perform single fetch from offset and add results
			Tuple<List<T>, String> fetch = callback.load(offset);
			size = fetch.a.size();
			rv.addAll(fetch.a);
			if (size == 0)
				// no results, set offset to -1
				return Tuple.create(-1, rv);
			else if (size < limit)
				// limit not reached, find more results; increment offset by limit
				return find(offset + limit, rv, callback);
			else
				// limit reached, return results; increment offset by limit
				return Tuple.create(offset + limit, rv);
		}
	}

	// list results
	@GET
	@Produces(APPLICATION_JSON)
	public Response list(
		@DefaultValue("0") @QueryParam("off") int offset,
		@Context UriInfo info) {

		// perform the recursive fetch for items
		Tuple<Integer, List<T>> rv = fetch(offset, new LoadCallback<T>() {
			@Override
			public Tuple<List<T>, String> load(int offset) {
				return dao.find(
					new Criterion[] { Offset.create(offset), Limit.create(limit) },
					new Class<?>[] { /* no load classes */});
			}
		});

		return Response.created(rv.a == -1 ? null :
				// set location of next page of results (i.e., next fetch)
				UriBuilder.fromUri(info.getAbsolutePath())
						.queryParam("off", rv.a /* toString */)
						.build())
				// set result entities
				.entity(rv.b)
				// build response
				.build();
	}

	// fetch limit
	@Inject
	@Named("limit")
	private int limit;
}