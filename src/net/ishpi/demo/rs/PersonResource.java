package net.ishpi.demo.rs;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import net.ishpi.demo.dao.PersonDao;
import net.ishpi.demo.db.Person;

import com.google.inject.Inject;

/**
 * Useful links and tutorials I used to create the code for koindex.js, communications.js and this file.
 * 
 * http://coenraets.org/blog/2011/12/restful-services-with-jquery-and-java-using-jax-rs-and-jersey/
 * http://api.jquery.com/jquery.ajax/
 * https://code.google.com/p/objectify-appengine/wiki/AnnotationReference
 * 
 * @author dnavarro
 *
 */
@Path("/person")
public class PersonResource extends FetchableResource<Person> {

	@Inject
	public PersonResource(PersonDao dao) {
		super(Person.class, dao);
	}
	
	@Path("add")
	@POST    
	public Person add(Person person){		
		dao.persist(person);
		return person;
	}
	
	@Path("all")
	@GET	
	public List<Person> all(){
		return dao.findAll();
	}
	
	@PUT 
	@Path("update/{id}")
    public Person update(@PathParam("id") Long id, Person person) { 		
		Person p = dao.find(id);
		p.updateFields(person);
		dao.persist(p);
		return p;		
	}
	
	@DELETE 
	@Path("delete/{id}")
    public void delete(@PathParam("id") Long id) {
        dao.delete(id);
    }
}